import request from '@/utils/request'

export default {

  // 获取前八条热门课程,获取前四个名师
  getListIndex() {
    return request({
      url: `/eduservice/indexFront/idnex`,
      method: 'get'
    })
  }
}
