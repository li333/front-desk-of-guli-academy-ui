import request from '@/utils/request'
export default {

  // 评论列表分页
  getPageList(page, limit, courseId) {
    return request({
      url: `/eduservice/eduComment/getCommentPage/${page}/${limit}`,
      method: 'get',
      params: { courseId }
    })
  },
  // 添加评论
  addComment(comment) {
    return request({
      url: `/eduservice/eduComment/addComment`,
      method: 'post',
      data: comment
    })
  }
}
