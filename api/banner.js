import request from '@/utils/request'

export default {

  // 查询所有的banner数据
  getBannerList() {
    return request({
      url: `/eduCms/bannerFront/getAllBanner`,
      method: 'get'
    })
  }
}
