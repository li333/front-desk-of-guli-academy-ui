import request from '@/utils/request'
export default {
  // 创建订单
  createOrders(courseId) {
    return request({
      url: `/eduOrder/order/createOrder/${courseId}`,
      method: 'post'
    })
  },
  // 根据订单id，获取订单信息
  getOrdersInfo(orderId) {
    return request({
      url: `/eduOrder/order/getOrder/${orderId}`,
      method: 'get'
    })
  },
  // 根据订单id，生成微信支付二维码
  createNatvie(orderNo) {
    return request({
      url: `/eduOrder/payLog/createWxQRcode/${orderNo}`,
      method: 'get'
    })
  },
  // 查询订单状态的方法
  // 生成二维码的方法
  queryPayStatus(orderNo) {
    return request({
      url: '/eduOrder/payLog/queryPayStatus/' + orderNo,
      method: 'get'
    })
  }

}
