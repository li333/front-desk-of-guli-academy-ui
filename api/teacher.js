import request from '@/utils/request'

export default {
  // 讲师分页查询
  getTeacherFrontList(page, limit) {
    return request({
      url: `/eduservice/teacherFront/getTeacherFrontList/${page}/${limit}`,
      method: 'get'
    })
  },
  // 根据id获取讲师所讲课程列表
  getTeacherInfo(id) {
    return request({
      url: `/eduservice/teacherFront/selectByTeacherId/${id}`,
      method: 'get'
    })
  }
}
