import request from '@/utils/request'

export default {
  // 根据邮箱发送验证码邮件
  getEmail(email) {
    return request({
      url: `/eduMsm/sendEmail/${email}`,
      method: 'get'
    })
  },
  // 用户注册
  register(formItem) {
    return request({
      url: `/educenter/register`,
      method: 'post',
      data: formItem
    })
  }

}
