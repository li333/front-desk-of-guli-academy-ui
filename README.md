NUXT目录结构

（1）资源目录 assets
 用于组织未编译的静态资源如 LESS、SASS 或 JavaScript。
（2）组件目录 components
用于组织应用的 Vue.js 组件。Nuxt.js 不会扩展增强该目录下 Vue.js 组件，即这些组件不会像页面组件那样有 asyncData 方法的特性。
（3）布局目录 layouts
用于组织应用的布局组件。
（4）页面目录 pages
用于组织应用的路由及视图。Nuxt.js 框架读取该目录下所有的 .vue 文件并自动生成对应的路由配置。
（5）插件目录 plugins
用于组织那些需要在 根vue.js应用 实例化之前需要运行的 Javascript 插件。
（6）nuxt.config.js 文件
nuxt.config.js 文件用于组织Nuxt.js 应用的个性化配置，以便覆盖默认配置。




# {{ name }}

> {{ description }}

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

